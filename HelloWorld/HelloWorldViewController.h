//
//  HelloWorldViewController.h
//  HelloWorld
//
//  Created by James Murphy on 7/17/2013.
//  Copyright (c) 2013 KazenMur Internet Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldViewController : UIViewController <UITextFieldDelegate>

@property (copy, nonatomic) NSString *userName;
@end
