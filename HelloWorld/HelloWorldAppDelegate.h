//
//  HelloWorldAppDelegate.h
//  HelloWorld
//
//  Created by James Murphy on 7/17/2013.
//  Copyright (c) 2013 KazenMur Internet Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
