//
//  main.m
//  HelloWorld
//
//  Created by James Murphy on 7/17/2013.
//  Copyright (c) 2013 KazenMur Internet Group. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HelloWorldAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HelloWorldAppDelegate class]));
    }
}
