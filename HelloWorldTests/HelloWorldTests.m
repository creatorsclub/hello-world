//
//  HelloWorldTests.m
//  HelloWorldTests
//
//  Created by James Murphy on 7/17/2013.
//  Copyright (c) 2013 KazenMur Internet Group. All rights reserved.
//

#import "HelloWorldTests.h"

@implementation HelloWorldTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in HelloWorldTests");
}

@end
